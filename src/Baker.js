import { Bread } from "./Bread";

export class Baker {
    /**
     * 
     * @param {} name 
     * @param {Number} supply 
     */
    constructor(name, supply) {
        this.name = name;
        this.flourSupply = supply;
    }
    /**
     * 
     * @param {Number} quantity 
     * @returns {Array}
     */
    bakeBreads(quantity) {
        let fournee = [];
        for (let index = 0; index < quantity; index++) {
            if (this.flourSupply > 0) {
                let pain = new Bread(200, 'Baguette')
                pain.cook(250);
                fournee.push(pain);
                this.flourSupply -= 1;
            }
        }
        return fournee;
    }
}