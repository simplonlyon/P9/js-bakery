export class FrenchPerson {
    /**
     * @param {Number} hunger 
     * @param {String} name 
     */
    constructor(hunger, name) {
        this.hunger = hunger;
        this.name = name;
    }

    /**
     * 
     * @param {Bread} bread Svp du pain
     */
    eat(bread){
        if (this.hunger > 0){
            this.hunger -= bread.weight;
        }
    }
}