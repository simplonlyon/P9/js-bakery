export class Bread {
    /**
     * @param {number} weight 
     * @param {string} type 
     */
    constructor(weight, type) {
        this.weight = weight;
        this.type = type;
        this.cooked = false;
    }
    /**
     * 
     * @param {number} temp La température de cuisson
     * @returns {boolean} True si la cuisson a réussi, false sinon
     */
    cook(temp) {
        if (!this.cooked && temp > 200 && temp < 400) {
            this.weight -= this.weight * 0.1;
            this.cooked = true;
            return true;
        }
        return false;
    };
}; 