import { Bread } from "./Bread";
import { FrenchPerson } from "./FrenchPerson";
import { Baker } from "./Baker";


let painCampagne = new Bread(500, "Pain de Campagne");

console.log(painCampagne);


console.log(painCampagne.cook(300));

console.log(painCampagne);

let michel = new FrenchPerson(1000, 'Michel')
console.log(michel);

michel.eat(painCampagne)

console.log(michel);

let boulanger = new Baker('Thierry', 200);

let fournee = boulanger.bakeBreads(20);

console.log(fournee);
