/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Baker.js":
/*!**********************!*\
  !*** ./src/Baker.js ***!
  \**********************/
/*! exports provided: Baker */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Baker", function() { return Baker; });
/* harmony import */ var _Bread__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Bread */ "./src/Bread.js");


class Baker {
    /**
     * 
     * @param {} name 
     * @param {Number} supply 
     */
    constructor(name, supply) {
        this.name = name;
        this.flourSupply = supply;
    }
    /**
     * 
     * @param {Number} quantity 
     * @returns {Array}
     */
    bakeBreads(quantity) {
        let fournee = [];
        for (let index = 0; index < quantity; index++) {
            if (this.flourSupply > 0) {
                let pain = new _Bread__WEBPACK_IMPORTED_MODULE_0__["Bread"](200, 'Baguette')
                pain.cook(250);
                fournee.push(pain);
                this.flourSupply -= 1;
            }
        }
        return fournee;
    }
}

/***/ }),

/***/ "./src/Bread.js":
/*!**********************!*\
  !*** ./src/Bread.js ***!
  \**********************/
/*! exports provided: Bread */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Bread", function() { return Bread; });
class Bread {
    /**
     * @param {number} weight 
     * @param {string} type 
     */
    constructor(weight, type) {
        this.weight = weight;
        this.type = type;
        this.cooked = false;
    }
    /**
     * 
     * @param {number} temp La température de cuisson
     * @returns {boolean} True si la cuisson a réussi, false sinon
     */
    cook(temp) {
        if (!this.cooked && temp > 200 && temp < 400) {
            this.weight -= this.weight * 0.1;
            this.cooked = true;
            return true;
        }
        return false;
    };
}; 

/***/ }),

/***/ "./src/FrenchPerson.js":
/*!*****************************!*\
  !*** ./src/FrenchPerson.js ***!
  \*****************************/
/*! exports provided: FrenchPerson */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FrenchPerson", function() { return FrenchPerson; });
class FrenchPerson {
    /**
     * @param {Number} hunger 
     * @param {String} name 
     */
    constructor(hunger, name) {
        this.hunger = hunger;
        this.name = name;
    }

    /**
     * 
     * @param {Bread} bread Svp du pain
     */
    eat(bread){
        if (this.hunger > 0){
            this.hunger -= bread.weight;
        }
    }
}

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Bread__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Bread */ "./src/Bread.js");
/* harmony import */ var _FrenchPerson__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FrenchPerson */ "./src/FrenchPerson.js");
/* harmony import */ var _Baker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Baker */ "./src/Baker.js");





let painCampagne = new _Bread__WEBPACK_IMPORTED_MODULE_0__["Bread"](500, "Pain de Campagne");

console.log(painCampagne);


console.log(painCampagne.cook(300));

console.log(painCampagne);

let michel = new _FrenchPerson__WEBPACK_IMPORTED_MODULE_1__["FrenchPerson"](1000, 'Michel')
console.log(michel);

michel.eat(painCampagne)

console.log(michel);

let boulanger = new _Baker__WEBPACK_IMPORTED_MODULE_2__["Baker"]('Thierry', 200);

let fournee = boulanger.bakeBreads(20);

console.log(fournee);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL0Jha2VyLmpzIiwid2VicGFjazovLy8uL3NyYy9CcmVhZC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvRnJlbmNoUGVyc29uLmpzIiwid2VicGFjazovLy8uL3NyYy9pbmRleC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxrREFBMEMsZ0NBQWdDO0FBQzFFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0VBQXdELGtCQUFrQjtBQUMxRTtBQUNBLHlEQUFpRCxjQUFjO0FBQy9EOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBeUMsaUNBQWlDO0FBQzFFLHdIQUFnSCxtQkFBbUIsRUFBRTtBQUNySTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOzs7QUFHQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQUE7QUFBQTtBQUFnQzs7QUFFekI7QUFDUDtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCLGVBQWUsT0FBTztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QixpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCLGtCQUFrQjtBQUM3QztBQUNBLCtCQUErQiw0Q0FBSztBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7Ozs7OztBQzdCQTtBQUFBO0FBQU87QUFDUDtBQUNBLGVBQWUsT0FBTztBQUN0QixlQUFlLE9BQU87QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QixpQkFBaUIsUUFBUTtBQUN6QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFOzs7Ozs7Ozs7Ozs7QUN2QkE7QUFBQTtBQUFPO0FBQ1A7QUFDQSxlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7Ozs7Ozs7QUNuQkE7QUFBQTtBQUFBO0FBQUE7QUFBZ0M7QUFDYztBQUNkOzs7QUFHaEMsdUJBQXVCLDRDQUFLOztBQUU1Qjs7O0FBR0E7O0FBRUE7O0FBRUEsaUJBQWlCLDBEQUFZO0FBQzdCOztBQUVBOztBQUVBOztBQUVBLG9CQUFvQiw0Q0FBSzs7QUFFekI7O0FBRUEiLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2luZGV4LmpzXCIpO1xuIiwiaW1wb3J0IHsgQnJlYWQgfSBmcm9tIFwiLi9CcmVhZFwiO1xuXG5leHBvcnQgY2xhc3MgQmFrZXIge1xuICAgIC8qKlxuICAgICAqIFxuICAgICAqIEBwYXJhbSB7fSBuYW1lIFxuICAgICAqIEBwYXJhbSB7TnVtYmVyfSBzdXBwbHkgXG4gICAgICovXG4gICAgY29uc3RydWN0b3IobmFtZSwgc3VwcGx5KSB7XG4gICAgICAgIHRoaXMubmFtZSA9IG5hbWU7XG4gICAgICAgIHRoaXMuZmxvdXJTdXBwbHkgPSBzdXBwbHk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFxuICAgICAqIEBwYXJhbSB7TnVtYmVyfSBxdWFudGl0eSBcbiAgICAgKiBAcmV0dXJucyB7QXJyYXl9XG4gICAgICovXG4gICAgYmFrZUJyZWFkcyhxdWFudGl0eSkge1xuICAgICAgICBsZXQgZm91cm5lZSA9IFtdO1xuICAgICAgICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgcXVhbnRpdHk7IGluZGV4KyspIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmZsb3VyU3VwcGx5ID4gMCkge1xuICAgICAgICAgICAgICAgIGxldCBwYWluID0gbmV3IEJyZWFkKDIwMCwgJ0JhZ3VldHRlJylcbiAgICAgICAgICAgICAgICBwYWluLmNvb2soMjUwKTtcbiAgICAgICAgICAgICAgICBmb3VybmVlLnB1c2gocGFpbik7XG4gICAgICAgICAgICAgICAgdGhpcy5mbG91clN1cHBseSAtPSAxO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmb3VybmVlO1xuICAgIH1cbn0iLCJleHBvcnQgY2xhc3MgQnJlYWQge1xuICAgIC8qKlxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSB3ZWlnaHQgXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHR5cGUgXG4gICAgICovXG4gICAgY29uc3RydWN0b3Iod2VpZ2h0LCB0eXBlKSB7XG4gICAgICAgIHRoaXMud2VpZ2h0ID0gd2VpZ2h0O1xuICAgICAgICB0aGlzLnR5cGUgPSB0eXBlO1xuICAgICAgICB0aGlzLmNvb2tlZCA9IGZhbHNlO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gdGVtcCBMYSB0ZW1ww6lyYXR1cmUgZGUgY3Vpc3NvblxuICAgICAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIHNpIGxhIGN1aXNzb24gYSByw6l1c3NpLCBmYWxzZSBzaW5vblxuICAgICAqL1xuICAgIGNvb2sodGVtcCkge1xuICAgICAgICBpZiAoIXRoaXMuY29va2VkICYmIHRlbXAgPiAyMDAgJiYgdGVtcCA8IDQwMCkge1xuICAgICAgICAgICAgdGhpcy53ZWlnaHQgLT0gdGhpcy53ZWlnaHQgKiAwLjE7XG4gICAgICAgICAgICB0aGlzLmNvb2tlZCA9IHRydWU7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfTtcbn07ICIsImV4cG9ydCBjbGFzcyBGcmVuY2hQZXJzb24ge1xuICAgIC8qKlxuICAgICAqIEBwYXJhbSB7TnVtYmVyfSBodW5nZXIgXG4gICAgICogQHBhcmFtIHtTdHJpbmd9IG5hbWUgXG4gICAgICovXG4gICAgY29uc3RydWN0b3IoaHVuZ2VyLCBuYW1lKSB7XG4gICAgICAgIHRoaXMuaHVuZ2VyID0gaHVuZ2VyO1xuICAgICAgICB0aGlzLm5hbWUgPSBuYW1lO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFxuICAgICAqIEBwYXJhbSB7QnJlYWR9IGJyZWFkIFN2cCBkdSBwYWluXG4gICAgICovXG4gICAgZWF0KGJyZWFkKXtcbiAgICAgICAgaWYgKHRoaXMuaHVuZ2VyID4gMCl7XG4gICAgICAgICAgICB0aGlzLmh1bmdlciAtPSBicmVhZC53ZWlnaHQ7XG4gICAgICAgIH1cbiAgICB9XG59IiwiaW1wb3J0IHsgQnJlYWQgfSBmcm9tIFwiLi9CcmVhZFwiO1xuaW1wb3J0IHsgRnJlbmNoUGVyc29uIH0gZnJvbSBcIi4vRnJlbmNoUGVyc29uXCI7XG5pbXBvcnQgeyBCYWtlciB9IGZyb20gXCIuL0Jha2VyXCI7XG5cblxubGV0IHBhaW5DYW1wYWduZSA9IG5ldyBCcmVhZCg1MDAsIFwiUGFpbiBkZSBDYW1wYWduZVwiKTtcblxuY29uc29sZS5sb2cocGFpbkNhbXBhZ25lKTtcblxuXG5jb25zb2xlLmxvZyhwYWluQ2FtcGFnbmUuY29vaygzMDApKTtcblxuY29uc29sZS5sb2cocGFpbkNhbXBhZ25lKTtcblxubGV0IG1pY2hlbCA9IG5ldyBGcmVuY2hQZXJzb24oMTAwMCwgJ01pY2hlbCcpXG5jb25zb2xlLmxvZyhtaWNoZWwpO1xuXG5taWNoZWwuZWF0KHBhaW5DYW1wYWduZSlcblxuY29uc29sZS5sb2cobWljaGVsKTtcblxubGV0IGJvdWxhbmdlciA9IG5ldyBCYWtlcignVGhpZXJyeScsIDIwMCk7XG5cbmxldCBmb3VybmVlID0gYm91bGFuZ2VyLmJha2VCcmVhZHMoMjApO1xuXG5jb25zb2xlLmxvZyhmb3VybmVlKTtcbiJdLCJzb3VyY2VSb290IjoiIn0=