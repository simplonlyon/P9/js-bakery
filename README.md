# js-bakery

## Exercice Boulangerie (webpack + classes)
1. Créer un projet npm js-bakery (faire le package.json, faire la structure du projet comme il faut (dossier src...), faire le gitignore tout ça)
2. Dans ce projet, installer webpack dans ce projet et le configurer si besoin
3. Créer une classe Bread qui va ressembler à ça
Faire le constructeur de cette classe en mettant la propriété cooked à false par défaut
![Diagramme de la classe Bread](./uml/bread.png)
4. La méthode cook va faire : Si la temperature donnée en argument est entre 200 et 400 > fait baisser le weight de 10%, passer la propriété cooked à true et return true
Si la température vaut autre chose, juste return false
Faire également que la méthode cook fasse quelque chose seulement si cooked est false
(tester le tout dans un index.js en faisant une instance de la classe et en déclenchant sa méthode cook et en faisant un console.log tout ça, comme d'hab)
5. Créer une classe FrenchPerson qui ressemblera à ça :
![Diagramme de la classe FrenchPerson](./uml/french.png)
Faire que la méthode eat fasse diminuer la faim de la personne du poid du pain mangé
Faire que la personne ne mange que si elle a faim
(dans le index.js, faire manger du pain à la personne pour tester)
6. Créer une classe Baker qui ressemblera à ça (sauf que c'est flourSupply, pas floorSupply et je peux pas modifier l'image) :
![Diagramme de la classe Baker](./uml/Baker.png)
Faire que la méthode bakeBreads crée un tableau vide puis fasse une boucle qui fera quantity tours et qui à chaque tour de boucle créera une instance de Bread, déclenchera sa méthode cook et le mettra dans le tableau. A la fin de la boucle, la méthode return le tableau de Bread
7. Faire en sorte que dans la boucle de bakeBread, chaque fois qu'on fait un pain, on baisse la valeur de la propriété flourSupply de 1 et que le boulanger fasse du pain uniquement s'il lui reste de la farine (flourSupply supérieur à 0)
Le diagramme de l'application complète :
![Diagramme de l'appli entière](./uml/bakery.jpg)
